use std::fmt;

#[derive(Debug, Clone, PartialEq)]
pub enum StateType {
    ON,
    OFF,
}

impl fmt::Display for StateType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(PartialEq)]
pub struct Socket {
    state: StateType,
    power: f64,
    info: String,
}

impl Socket {
    pub fn new(state: StateType, power: f64, info: String) -> Socket {
        Socket { state, power, info }
    }

    pub fn on(&mut self) -> &mut Self {
        self.state = StateType::ON;
        self
    }

    pub fn off(&mut self) -> &mut Self {
        self.state = StateType::OFF;
        self
    }

    pub fn get_description(&self) -> String {
        if self.state == StateType::ON {
            format!("{}", self.info)
        } else {
            String::from("Socket is turned off. If you want a description, you should enable the socket")
        }
    }

    pub fn get_state(&self) -> StateType {
        self.state.clone()
    }

    pub fn get_power(&self) -> String {
        if self.state == StateType::ON {
            format!("{}", self.power)
        } else {
            String::from("Socket is turned off. If you want a description, you should enable the socket")
        }
    }
}

pub struct Thermometer {
    temperature: i8
}

impl Thermometer {
    pub fn new(temperature: i8) -> Thermometer {
        Thermometer { temperature }
    }

    pub fn get_temperature(&self) -> i8 {
        self.temperature
    }
}

pub struct House {
    pub socket: Socket,
    pub thermometer: Thermometer,
}

impl House {
    pub fn new(socket: Socket, thermometer: Thermometer) -> House {
        House { socket, thermometer }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn turn_on_socket() {
        let mut socket = Socket::new(StateType::OFF, 3.5, String::from("model A"));

        socket.on();
        assert_eq!(StateType::ON, socket.get_state())
    }

    #[test]
    fn turn_off_socket() {
        let mut socket = Socket::new(StateType::ON, 3.5, String::from("model A"));

        socket.off();
        assert_eq!(StateType::OFF, socket.get_state())
    }

    #[test]
    fn get_socket_info_with_on_state() {
        let socket = Socket::new(StateType::ON, 3.5, String::from("model A"));

        assert_eq!(String::from("model A"), socket.get_description())
    }

    #[test]
    fn get_socket_info_with_off_state() {
        let socket = Socket::new(StateType::OFF, 3.5, String::from("model A"));

        assert_ne!(String::from("model A"), socket.get_description())
    }

    #[test]
    fn get_temperature() {
        let thermometer = Thermometer::new(25);

        assert_eq!(25,thermometer.get_temperature())
    }
}
