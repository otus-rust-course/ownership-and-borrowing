use ownership_and_borrowing::{ House, Socket, Thermometer, StateType };

fn main() {
  let socket = Socket::new(StateType::OFF, 3.5, String::from("Model A"));
  let thermometer = Thermometer::new(22);
  let mut house = House::new(socket, thermometer);

  // work with socket
  // get info about socket, when socket is turned off
  println!("State of socket: {}", house.socket.get_state());
  println!("Socket's info: {}\n\n", house.socket.get_description());

  // get info about socket, when socket is enabled
  house.socket.on();
  println!("State of socket: {}", house.socket.get_state());
  println!("Socket's info: {}", house.socket.get_description());

  // get socket's power, when socket is turned off
  println!("State of socket: {}", house.socket.get_state());
  println!("Socket's info: {}\n\n", house.socket.get_power());

  // get socket's power, when socket is enabled
  house.socket.on();
  println!("State of socket: {}", house.socket.get_state());
  println!("Socket's info: {}", house.socket.get_power());

  // work with thermometer
  println!("Temperature: {}", house.thermometer.get_temperature());
}